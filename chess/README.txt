README file of Chess Game project created 25/03/2021

# Chess Game 
***
this is a chess game for a single player to play against computer.

## Table of Contents
1. [General Info](#general-info)
2. [Prerequisite](#prerequisite)
3. [Installation](#installation)
4. [Installation](#installation)
5. [Collaboration](#collaboration)
6. [FAQS(help)](#FAQS(help))


## General Info
***
This is a web-based chess game for a single player to play against computer.this is completly free to play.

## Technologies
***
A list of technologies used within the project:
# Development
* [PyCharm Community Edition] Version 2020.3.2
# Deployment 
Built-in fuctionality of pycharm for Deployment  
* [PyCharm Community Edition] Version 2020.3.2

## prerequisite
***
*laptop or desktop computer or mobile 
*internet 


## Installation
***
this project is basically a website not a software, so no installation is required. you can use it on any browser. 
```

## Collaboration
***
Instructions on how to collaborate with your project.
1- open the website in any browser of your choice 
2- user interface appears 
3- click on sigle player
4- click on start game 
5- chess board appears and there you go 
6- play game and have fun!

# FAQS(help)
* if you dont know how to play chess visit https://www.chess.com/lessons 



